package com.sahaj.poc.flink;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BatchWordCountExampleTest {

	private BatchWordCountExample wordCountExampleTest;
	
	@Before
	public void setup() {
		wordCountExampleTest = new BatchWordCountExample();
	}
	
	@Test
	public void testDoWordCount() throws Exception {
		
		Map<String, Integer> expectedWordCountMap = new HashMap<>();
		expectedWordCountMap.put("apache",1);
		expectedWordCountMap.put("flink",1);
		expectedWordCountMap.put("hello",4);
		expectedWordCountMap.put("to",1);
		expectedWordCountMap.put("a",1);
		expectedWordCountMap.put("is",1);
		expectedWordCountMap.put("represent",1);
		expectedWordCountMap.put("sample",1);
		expectedWordCountMap.put("streaming",1);
		expectedWordCountMap.put("text",1);
		expectedWordCountMap.put("this",1);
		Map<String, Integer> resultWordCountMap = wordCountExampleTest.getWordCount("Hello! This is a text sample",
				"to represent Apache Flink streaming", 
				"Hello Hello Hello");
		Assert.assertEquals(expectedWordCountMap, resultWordCountMap);
	}
}
