package com.sahaj.poc.flink;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.DataStreamUtils;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StreamWordCountExampleTest {

	private StreamWordCountExample streamWordCountExample;
	
	@Before
	public void setup() {
		streamWordCountExample = new StreamWordCountExample();
	}
	
	@Test
	public void testDoStreamingWordCount() throws Exception {
		
		Map<String, Integer> expectedWordCountMap = new HashMap<>();
		expectedWordCountMap.put("apache",1);
		expectedWordCountMap.put("flink",1);
		expectedWordCountMap.put("hello",4);
		expectedWordCountMap.put("to",1);
		expectedWordCountMap.put("a",1);
		expectedWordCountMap.put("is",1);
		expectedWordCountMap.put("represent",1);
		expectedWordCountMap.put("sample",1);
		expectedWordCountMap.put("streaming",1);
		expectedWordCountMap.put("text",1);
		expectedWordCountMap.put("this",1);
		
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		DataStreamSource<String> sentenceStream = env.fromElements("Hello! This is a text sample",
				"to represent Apache Flink streaming", 
				"Hello Hello Hello");
		
		DataStream<Tuple2<String, Integer>> resultDataStream = streamWordCountExample.doStreamingWordCount(sentenceStream);
		// resultDataStream.print();
		Iterator<Tuple2<String, Integer>> resultIterator = DataStreamUtils.collect(resultDataStream);
		Iterable<Tuple2<String, Integer>> resultIterable = () -> resultIterator;
		
		Map<String, Integer> resultWordCountMap = new HashMap<>();
		for(Tuple2<String, Integer> tuple : resultIterable) {
			resultWordCountMap.put(tuple.f0, tuple.f1);
		}
		
		Assert.assertEquals(expectedWordCountMap, resultWordCountMap);
	}
}
