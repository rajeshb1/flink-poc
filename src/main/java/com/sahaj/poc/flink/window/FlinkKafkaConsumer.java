package com.sahaj.poc.flink.window;

import java.util.Properties;

import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;

public class FlinkKafkaConsumer {
	
	public static FlinkKafkaConsumer011<InputMessage> createInputMessageConsumer(
			String topic,
			String kafkaAddress,
			String kafkaGroup) {
		
		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", kafkaAddress);
		properties.setProperty("group.id", kafkaGroup);
		FlinkKafkaConsumer011<InputMessage> consumer = 
				new FlinkKafkaConsumer011<InputMessage>(topic,
						new InputMessageDeserializationSchema(), properties);
		return consumer;
	}
}
