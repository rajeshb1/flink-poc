package com.sahaj.poc.flink.window;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.flink.api.common.functions.AggregateFunction;

public class BackupAggregator implements AggregateFunction<InputMessage, List<InputMessage>, Backup> {

	private static final long serialVersionUID = 1L;

	@Override
	public List<InputMessage> createAccumulator() {
		return new ArrayList<>();
	}

	@Override
	public List<InputMessage> add(InputMessage inputMessage, List<InputMessage> inputMessages) {
		inputMessages.add(inputMessage);
		return inputMessages;
	}

	@Override
	public Backup getResult(List<InputMessage> inputMessages) {
		return new Backup(inputMessages, LocalDateTime.now());
	}

	@Override
	public List<InputMessage> merge(List<InputMessage> inputMessages, List<InputMessage> acc1) {
		inputMessages.addAll(acc1);
		return inputMessages;
	}
}