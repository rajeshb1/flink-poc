package com.sahaj.poc.flink.window;

import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;

public class FlinkKafkaProducer {
	
	public static FlinkKafkaProducer011<Backup> createBackupProducer(
			String topic,
			String kafkaAddress) {
		
		return new FlinkKafkaProducer011<Backup>(kafkaAddress, topic, new BackupSerializationSchema());
	}
}
