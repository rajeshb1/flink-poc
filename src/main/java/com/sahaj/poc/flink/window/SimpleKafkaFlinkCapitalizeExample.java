package com.sahaj.poc.flink.window;

import java.util.Properties;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;

public class SimpleKafkaFlinkCapitalizeExample {

	private static String BOOTSTRAP_SERVER = "localhost:9092";
	private static String INPUT_TOPIC = "input.topic";
	private static String OUTPUT_TOPIC = "output.topic";

	public static void main(String[] args) {
		
		try {
			capitalize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void capitalize() throws Exception {

		StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
		FlinkKafkaConsumer011<String> flinkKafkaConsumer = createStringConsumerForTopic();
		DataStream<String> stringInputStream = environment.addSource(flinkKafkaConsumer);

		FlinkKafkaProducer011<String> flinkKafkaProducer = createStringProducer();

		stringInputStream.map(new WordsCapitalizer()).addSink(flinkKafkaProducer);

		environment.execute();
	}

	public static FlinkKafkaConsumer011<String> createStringConsumerForTopic() {

		Properties props = new Properties();
		props.setProperty("bootstrap.servers", BOOTSTRAP_SERVER);
		FlinkKafkaConsumer011<String> consumer = 
				new FlinkKafkaConsumer011<>(INPUT_TOPIC, new SimpleStringSchema(), props);
		return consumer;
	}

	public static FlinkKafkaProducer011<String> createStringProducer() {

		return new FlinkKafkaProducer011<>(BOOTSTRAP_SERVER, OUTPUT_TOPIC, new SimpleStringSchema());
	}
}

class WordsCapitalizer implements MapFunction<String, String> {

	private static final long serialVersionUID = 1L;

	@Override
	public String map(String s) {
		return s.toUpperCase();
	}
}
