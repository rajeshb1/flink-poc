package com.sahaj.poc.flink.window;

import javax.annotation.Nullable;

import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;

public class InputMessageTimestampAssigner implements AssignerWithPunctuatedWatermarks<InputMessage> {

	private static final long serialVersionUID = 1L;

	@Override
	public long extractTimestamp(InputMessage element, long previousElementTimestamp) {
		return element.getEventTimestamp();
	}

	@Nullable
	@Override
	public Watermark checkAndGetNextWatermark(InputMessage lastElement, long extractedTimestamp) {
		return new Watermark(extractedTimestamp - 1500);
	}
}