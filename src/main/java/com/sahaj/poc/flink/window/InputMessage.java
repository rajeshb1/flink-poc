package com.sahaj.poc.flink.window;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonSerialize
@JsonIgnoreProperties(ignoreUnknown = true)
public class InputMessage {

	String sender;
	String recipient;
	String message;
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public long getEventTimestamp() {
		return System.currentTimeMillis() - 10;
	}
}