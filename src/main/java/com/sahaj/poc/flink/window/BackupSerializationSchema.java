package com.sahaj.poc.flink.window;

import org.apache.flink.api.common.serialization.SerializationSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class BackupSerializationSchema implements SerializationSchema<Backup> {

	private static final long serialVersionUID = 1L;
	ObjectMapper objectMapper;
	Logger logger = LoggerFactory.getLogger(BackupSerializationSchema.class);

	@Override
	public byte[] serialize(Backup backupMessage) {
		if (objectMapper == null) {
			objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
		}
		try {
			return objectMapper.writeValueAsString(backupMessage).getBytes();
		} catch (Exception e) {
			logger.error("Failed to parse JSON", e);
		}
		return new byte[0];
	}
}