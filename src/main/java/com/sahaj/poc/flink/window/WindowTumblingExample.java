package com.sahaj.poc.flink.window;

import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;

public class WindowTumblingExample {

	public static void keyByBackup(
			String kafkaAddress,
			String inputTopic,
			String outputTopic,
			String consumerGroup) throws Exception {
		
		StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

		environment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

		FlinkKafkaConsumer011<InputMessage> flinkKafkaConsumer = 
				FlinkKafkaConsumer.createInputMessageConsumer(inputTopic, kafkaAddress, consumerGroup);
		flinkKafkaConsumer.setStartFromEarliest();

		flinkKafkaConsumer.assignTimestampsAndWatermarks(new InputMessageTimestampAssigner());
		FlinkKafkaProducer011<Backup> flinkKafkaProducer = 
				FlinkKafkaProducer.createBackupProducer(outputTopic, kafkaAddress);

		DataStream<InputMessage> inputMessagesStream = environment.addSource(flinkKafkaConsumer);

		/*
		inputMessagesStream.keyBy("sender")
			.window(TumblingEventTimeWindows.of(Time.seconds(20)))
			.aggregate(new BackupAggregator())
			.addSink(flinkKafkaProducer);
		*/
		inputMessagesStream.keyBy("sender")
			.countWindow(2)
			.aggregate(new BackupAggregator())
			.addSink(flinkKafkaProducer);
		environment.execute();
	}
	
	public static void main(String[] args) {
		
		String inputTopic = "input4";
		String outputTopic = "output4";
		String consumerGroup = "test";
		String kafkaAddress = "localhost:9092";
		
		try {
			keyByBackup(kafkaAddress, inputTopic, outputTopic, consumerGroup);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
