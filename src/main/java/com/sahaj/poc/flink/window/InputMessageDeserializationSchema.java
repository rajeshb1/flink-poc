package com.sahaj.poc.flink.window;

import java.io.IOException;

import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class InputMessageDeserializationSchema implements DeserializationSchema<InputMessage> {

	private static final long serialVersionUID = 1L;
	static ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

	@Override
	public InputMessage deserialize(byte[] bytes) throws IOException {
		return objectMapper.readValue(bytes, InputMessage.class);
	}

	@Override
	public boolean isEndOfStream(InputMessage inputMessage) {
		return false;
	}

	@Override
	public TypeInformation<InputMessage> getProducedType() {
		return TypeInformation.of(InputMessage.class);
	}
}