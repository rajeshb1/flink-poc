package com.sahaj.poc.flink.window;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonProperty;

public class Backup {
	
    @JsonProperty("inputMessages")
    List<InputMessage> inputMessages;
    
    @JsonProperty("backupTimestamp")
    LocalDateTime backupTimestamp;
    
    @JsonProperty("uuid")
    UUID uuid;
 
    public Backup(List<InputMessage> inputMessages, 
      LocalDateTime backupTimestamp) {
        this.inputMessages = inputMessages;
        this.backupTimestamp = backupTimestamp;
        this.uuid = UUID.randomUUID();
    }

	public List<InputMessage> getInputMessages() {
		return inputMessages;
	}

	public void setInputMessages(List<InputMessage> inputMessages) {
		this.inputMessages = inputMessages;
	}

	public LocalDateTime getBackupTimestamp() {
		return backupTimestamp;
	}

	public void setBackupTimestamp(LocalDateTime backupTimestamp) {
		this.backupTimestamp = backupTimestamp;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
}