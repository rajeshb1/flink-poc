package com.sahaj.poc.flink.example.sensor;

import org.apache.flink.api.common.serialization.SerializationSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.sahaj.poc.flink.window.BackupSerializationSchema;

public class SensorSerializationSchema implements SerializationSchema<Sensor> {

	private static final long serialVersionUID = 1L;
	ObjectMapper objectMapper;
	Logger logger = LoggerFactory.getLogger(BackupSerializationSchema.class);

	@Override
	public byte[] serialize(Sensor sensor) {
		if (objectMapper == null) {
			objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
		}
		try {
			return objectMapper.writeValueAsString(sensor).getBytes();
		} catch (Exception e) {
			logger.error("Failed to parse JSON", e);
		}
		return new byte[0];
	}
}
