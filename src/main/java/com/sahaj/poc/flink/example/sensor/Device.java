package com.sahaj.poc.flink.example.sensor;

public enum Device {
	
	LIGHT(1), FAN(2), TV(3), FRIDGE(4), WASHING_MACHINE(5);
	
	private int unitConsumption;
	
	Device(int unitConsumption) {
		this.unitConsumption = unitConsumption;
	}

	public int getUnitConsumption() {
		return unitConsumption;
	}
}
