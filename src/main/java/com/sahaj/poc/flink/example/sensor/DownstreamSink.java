package com.sahaj.poc.flink.example.sensor;

import org.apache.flink.streaming.api.functions.sink.PrintSinkFunction;

public class DownstreamSink extends PrintSinkFunction<Output> {

	private static final long serialVersionUID = 1L;

	@Override
	public void invoke(Output record) {
		try {
			System.out.println(record.toJson());
		} catch (Exception e) {
			super.invoke(record);
		}
	}
}
