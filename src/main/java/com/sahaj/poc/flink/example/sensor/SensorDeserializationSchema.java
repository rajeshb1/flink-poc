package com.sahaj.poc.flink.example.sensor;

import java.io.IOException;

import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class SensorDeserializationSchema implements DeserializationSchema<Sensor> {

	private static final long serialVersionUID = 1L;
	static ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

	@Override
	public Sensor deserialize(byte[] bytes) throws IOException {
		return objectMapper.readValue(bytes, Sensor.class);
	}

	@Override
	public boolean isEndOfStream(Sensor sensor) {
		return false;
	}

	@Override
	public TypeInformation<Sensor> getProducedType() {
		return TypeInformation.of(Sensor.class);
	}
}