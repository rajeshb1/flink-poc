package com.sahaj.poc.flink.example.sensor;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

public class FlinkSensorDataPipeline {

	public static void main(String[] args) {
		
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.setStreamTimeCharacteristic(TimeCharacteristic.IngestionTime);
		// env.enableCheckpointing(100000l, CheckpointingMode.EXACTLY_ONCE);
		
		DataStream<Sensor> sensorDataStream = SensorSourceStream.getSource(env, 10);
		DataStream<Tuple2<String, Integer>> electricityMonitorStream = 
				ElectricityMonitorStream.getSource(env, 2);
		DownstreamSink downstreamSink = new DownstreamSink();
		
		sensorDataStream.print();
		electricityMonitorStream.print();
		
		sensorDataStream.join(electricityMonitorStream)
			.where(new SensorKeySelector()).equalTo(new ElectricityMonitorKeySelector())
			//.window(TumblingEventTimeWindows.of(Time.seconds(30)))
			.window(SlidingEventTimeWindows.of(Time.hours(1), Time.seconds(10)))
			.apply(customJoinFunction())
			.addSink(downstreamSink);
		
		try {
			env.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/* ----- SAMPLE OUTPUT ----- 
		 	1> (FAN,2)
			2> (FRIDGE,4)
			3> (LIGHT,1)
			4> (WASHING_MACHINE,5)
			1> Sensor [name=TV, temp=1, eventTimestamp=1590059907413]
			1> (FAN,2)
			2> (FAN,2)
			3> (TV,3)
			4> (FAN,2)
			1> (LIGHT,1)
			2> Sensor [name=WASHING_MACHINE, temp=15, eventTimestamp=1590059917414]
			2> (TV,3)
			3> (WASHING_MACHINE,5)
			{"device":"WASHING_MACHINE","temp":15,"electricityConsumption":5}
			{"device":"WASHING_MACHINE","temp":15,"electricityConsumption":5}
			{"device":"TV","temp":1,"electricityConsumption":3}
			{"device":"TV","temp":1,"electricityConsumption":3}
		 */
	}
	
	private static JoinFunction<Sensor, Tuple2<String, Integer>, Output> customJoinFunction() {
		return new JoinFunction<Sensor, Tuple2<String, Integer>, Output>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Output join(Sensor sensor, Tuple2<String, Integer> electricityData) {
				return new Output(sensor.getDevice().toString(), sensor.getTemp(), electricityData.f1);
			}
		};
	}
}

class SensorKeySelector implements KeySelector<Sensor, String> {
	
	private static final long serialVersionUID = 1L;

	@Override
	public String getKey(Sensor sensor) {
		return sensor.getDevice().toString();
	}
}

class ElectricityMonitorKeySelector implements KeySelector<Tuple2<String, Integer>, String> {
	
	private static final long serialVersionUID = 1L;

	@Override
	public String getKey(Tuple2<String, Integer> value) {
		return value.f0;
	}
}
