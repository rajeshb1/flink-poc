package com.sahaj.poc.flink.example.sensor;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class SensorSourceStream implements Iterator<Sensor>, Serializable {

	private static final long serialVersionUID = 1L;
	private final Random rnd = new Random(hashCode());
	
	private int delay;
	
	public SensorSourceStream() {
		this(1);
	}

	public SensorSourceStream(int delay) {
		this.delay = delay;
	}
	
	@Override
	public boolean hasNext() {
		return true;
	}

	@Override
	public Sensor next() {
		try {
			TimeUnit.SECONDS.sleep(this.delay);
		} catch (InterruptedException e) {
		}
		return new Sensor(Device.values()[rnd.nextInt(Device.values().length)],
				rnd.nextInt(70),
				System.currentTimeMillis());
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	public static DataStream<Sensor> getSource(StreamExecutionEnvironment env, int delay) {
		return env.fromCollection(
				new SensorSourceStream(delay),
				TypeInformation.of(new TypeHint<Sensor>() {
		}));
	}
}
