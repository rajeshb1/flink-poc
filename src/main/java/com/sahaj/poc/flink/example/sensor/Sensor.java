package com.sahaj.poc.flink.example.sensor;

public class Sensor {

	private Device device;
	private int temp;
	private long eventTimestamp;
	
	public Sensor() {
	}
	
	public Sensor(Device device, int temp, long eventTimestamp) {
		this.device = device;
		this.temp = temp;
		this.eventTimestamp = eventTimestamp;
	}
	
	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}
	
	public int getTemp() {
		return temp;
	}

	public void setTemp(int temp) {
		this.temp = temp;
	}

	public long getEventTimestamp() {
		return eventTimestamp;
	}

	public void setEventTimestamp(long eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Sensor [name=");
		builder.append(device);
		builder.append(", temp=");
		builder.append(temp);
		builder.append(", eventTimestamp=");
		builder.append(eventTimestamp);
		builder.append("]");
		return builder.toString();
	}
}
