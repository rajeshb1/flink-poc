package com.sahaj.poc.flink.example.sensor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Output {

	private String device;
	private int temp;
	private int electricityConsumption;
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public Output() {
	}

	public Output(String device, int temp, int electricityConsumption) {
		this.device = device;
		this.temp = temp;
		this.electricityConsumption = electricityConsumption;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public int getTemp() {
		return temp;
	}

	public void setTemp(int temp) {
		this.temp = temp;
	}

	public int getElectricityConsumption() {
		return electricityConsumption;
	}

	public void setElectricityConsumption(int electricityConsumption) {
		this.electricityConsumption = electricityConsumption;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Output [device=");
		builder.append(device);
		builder.append(", temp=");
		builder.append(temp);
		builder.append(", electricityConsumption=");
		builder.append(electricityConsumption);
		builder.append("]");
		return builder.toString();
	}

	public String toJson() throws JsonProcessingException {
		return objectMapper.writeValueAsString(this);
	}
}
