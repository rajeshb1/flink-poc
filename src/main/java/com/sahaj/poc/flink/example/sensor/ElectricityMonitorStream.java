package com.sahaj.poc.flink.example.sensor;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class ElectricityMonitorStream implements Iterator<Tuple2<String, Integer>>, Serializable {

	private static final long serialVersionUID = 1L;
	private final Random rnd = new Random(hashCode());
	
	private int delay;
	
	public ElectricityMonitorStream() {
		this(1);
	}

	public ElectricityMonitorStream(int delay) {
		this.delay = delay;
	}
	
	@Override
	public boolean hasNext() {
		return true;
	}

	@Override
	public Tuple2<String, Integer> next() {
		try {
			TimeUnit.SECONDS.sleep(this.delay);
		} catch (InterruptedException e) {
		}
		Device randomDevice = Device.values()[rnd.nextInt(Device.values().length)];
		return new Tuple2<String, Integer>(randomDevice.toString(), randomDevice.getUnitConsumption());
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	public static DataStream<Tuple2<String, Integer>> getSource(StreamExecutionEnvironment env, int delay) {
		return env.fromCollection(
				new ElectricityMonitorStream(delay),
				TypeInformation.of(new TypeHint<Tuple2<String, Integer>>() {
		}));
	}
}