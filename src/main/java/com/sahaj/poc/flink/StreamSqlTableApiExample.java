package com.sahaj.poc.flink;

import java.util.ArrayList;
import java.util.List;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

public class StreamSqlTableApiExample {

	public static void main(String[] args) {

		// Get the stream and table environments.
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		StreamTableEnvironment tEnv = StreamTableEnvironment.getTableEnvironment(env);
		env.setParallelism(1);

		// Provide a sample static data set of the rates history table.
		List<Tuple2<String, Long>> ratesHistoryData = new ArrayList<>();
		ratesHistoryData.add(Tuple2.of("USD", 50L));
		ratesHistoryData.add(Tuple2.of("EUR", 100L));
		ratesHistoryData.add(Tuple2.of("YEN", 500L));
		ratesHistoryData.add(Tuple2.of("EUR", 200L));
		ratesHistoryData.add(Tuple2.of("USD", 100L));

		// Create and register an example table using the sample data set.
		DataStream<Tuple2<String, Long>> ratesHistoryStream = env.fromCollection(ratesHistoryData);
		Table ratesHistory = tEnv.fromDataStream(ratesHistoryStream, "r_currency, r_rate, r_proctime.proctime");
		tEnv.registerTable("RatesHistory", ratesHistory);

		String query = "SELECT r_currency, AVG(r_rate) FROM RatesHistory"
				+ " GROUP BY r_currency";
		Table table = tEnv.sqlQuery(query);

		tEnv.toRetractStream(table, Row.class).print();
		
		try {
			env.execute();	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		/* -------STREAM OUTPUT OF AVERAGE VALUE-----
		
		(true,USD,50)
		(true,EUR,100)
		(true,YEN,500)
		(false,EUR,100)
		(true,EUR,150)
		(false,USD,50)
		(true,USD,75)
		
		 */
	}
	
	
}
