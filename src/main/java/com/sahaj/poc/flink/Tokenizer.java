package com.sahaj.poc.flink;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

public class Tokenizer implements FlatMapFunction<String, Tuple2<String, Integer>> {

	private static final long serialVersionUID = 8848938653056717821L;

	@Override
	public void flatMap(String sentence, Collector<Tuple2<String, Integer>> out) {
		String[] tokens = sentence.toLowerCase().split("\\W+");
		for (String token : tokens) {
			if (token.length() > 0) {
				out.collect(new Tuple2<String, Integer>(token, 1));
			}
		}
	}
}
