package com.sahaj.poc.flink;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.typeutils.TupleTypeInfo;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;

public class StreamWordCountExample {

    public DataStream<Tuple2<String, Integer>> doStreamingWordCount(DataStreamSource<String> sentenceStream) {
		return sentenceStream
                .flatMap(new Tokenizer())
                .returns(TupleTypeInfo.getBasicTupleTypeInfo( String.class, Integer.class ) )
                .keyBy(0)
                // .timeWindowAll(Time.seconds(5))
                .sum(1);
	}
}
