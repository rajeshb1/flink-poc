package com.sahaj.poc.flink.join;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Random;


@SuppressWarnings("serial")
public class WindowJoinSampleData {

	static final String[] NAMES = {"tom", "jerry", "alice", "bob", "john", "grace"};
	static final int GRADE_COUNT = 5;
	static final int SALARY_MAX = 10000;

	/**
	 * Continuously generates (name, grade).
	 */
	public static class GradeSource implements Iterator<Tuple2<String, Integer>>, Serializable {

		private final Random rnd = new Random(hashCode());

		@Override
		public boolean hasNext() {
			return true;
		}

		@Override
		public Tuple2<String, Integer> next() {
			return new Tuple2<>(NAMES[rnd.nextInt(NAMES.length)], rnd.nextInt(GRADE_COUNT) + 1);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		public static DataStream<Tuple2<String, Integer>> getSource(StreamExecutionEnvironment env, long rate) {
			return env.fromCollection(new ThrottledIterator<>(new GradeSource(), rate),
					TypeInformation.of(new TypeHint<Tuple2<String, Integer>>(){}));
		}
	}

	/**
	 * Continuously generates (name, salary).
	 */
	public static class SalarySource implements Iterator<Tuple2<String, Integer>>, Serializable {

		private final Random rnd = new Random(hashCode());

		@Override
		public boolean hasNext() {
			return true;
		}

		@Override
		public Tuple2<String, Integer> next() {
			return new Tuple2<>(NAMES[rnd.nextInt(NAMES.length)], rnd.nextInt(SALARY_MAX) + 1);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		public static DataStream<Tuple2<String, Integer>> getSource(StreamExecutionEnvironment env, long rate) {
			return env.fromCollection(new ThrottledIterator<>(new SalarySource(), rate),
					TypeInformation.of(new TypeHint<Tuple2<String, Integer>>(){}));
		}
	}
}

class ThrottledIterator<T> implements Iterator<T>, Serializable {

	private static final long serialVersionUID = 1L;

	private final Iterator<T> source;

	private final long sleepBatchSize;
	private final long sleepBatchTime;

	private long lastBatchCheckTime;
	private long num;

	public ThrottledIterator(Iterator<T> source, long elementsPerSecond) {
		this.source = requireNonNull(source);

		if (!(source instanceof Serializable)) {
			throw new IllegalArgumentException("source must be java.io.Serializable");
		}

		if (elementsPerSecond >= 100) {
			// how many elements would we emit per 50ms
			this.sleepBatchSize = elementsPerSecond / 20;
			this.sleepBatchTime = 50;
		}
		else if (elementsPerSecond >= 1) {
			// how long does element take
			this.sleepBatchSize = 1;
			this.sleepBatchTime = 1000 / elementsPerSecond;
		}
		else {
			throw new IllegalArgumentException("'elements per second' must be positive and not zero");
		}
	}

	@Override
	public boolean hasNext() {
		return source.hasNext();
	}

	@Override
	public T next() {
		// delay if necessary
		if (lastBatchCheckTime > 0) {
			if (++num >= sleepBatchSize) {
				num = 0;

				final long now = System.currentTimeMillis();
				final long elapsed = now - lastBatchCheckTime;
				if (elapsed < sleepBatchTime) {
					try {
						Thread.sleep(sleepBatchTime - elapsed);
					} catch (InterruptedException e) {
						// restore interrupt flag and proceed
						Thread.currentThread().interrupt();
					}
				}
				lastBatchCheckTime = now;
			}
		} else {
			lastBatchCheckTime = System.currentTimeMillis();
		}

		return source.next();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}