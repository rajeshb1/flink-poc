package com.sahaj.poc.flink.join;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import com.sahaj.poc.flink.join.WindowJoinSampleData.GradeSource;
import com.sahaj.poc.flink.join.WindowJoinSampleData.SalarySource;

public class WindowJoinExample {

	public static void main(String[] args) throws Exception {

		final long windowSize = 2000L;
		final long rate = 2L;

		System.out.println("Using windowSize=" + windowSize + ", data rate=" + rate);

		// obtain execution environment, run this example in "ingestion time"
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.setStreamTimeCharacteristic(TimeCharacteristic.IngestionTime);
		// create the data sources for both grades and salaries
		DataStream<Tuple2<String, Integer>> grades = GradeSource.getSource(env, rate);
		DataStream<Tuple2<String, Integer>> salaries = SalarySource.getSource(env, rate);
		// run the actual window join program
		// for testability, this functionality is in a separate method.
		DataStream<Tuple3<String, Integer, Integer>> joinedStream = runWindowJoin(grades, salaries, windowSize);
		// print the results with a single thread, rather than in parallel
		joinedStream.print().setParallelism(1);
		// execute program
		env.execute("Windowed Join Example");
	}

	public static DataStream<Tuple3<String, Integer, Integer>> runWindowJoin(DataStream<Tuple2<String, Integer>> grades,
			DataStream<Tuple2<String, Integer>> salaries, long windowSize) {

		return grades
				.join(salaries)
				.where(new NameKeySelector()).equalTo(new NameKeySelector())
				.window(TumblingEventTimeWindows.of(Time.milliseconds(windowSize)))
				.apply(customJoinFunction());
	}

	private static JoinFunction<Tuple2<String, Integer>, Tuple2<String, Integer>, Tuple3<String, Integer, Integer>> customJoinFunction() {
		return new JoinFunction<Tuple2<String, Integer>, Tuple2<String, Integer>, Tuple3<String, Integer, Integer>>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Tuple3<String, Integer, Integer> join(Tuple2<String, Integer> first,
					Tuple2<String, Integer> second) {
				return new Tuple3<String, Integer, Integer>(first.f0, first.f1, second.f1);
			}
		};
	}

	private static class NameKeySelector implements KeySelector<Tuple2<String, Integer>, String> {
		private static final long serialVersionUID = 1L;

		@Override
		public String getKey(Tuple2<String, Integer> value) {
			return value.f0;
		}
	}
}
