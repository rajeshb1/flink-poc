package com.sahaj.poc.flink;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple2;

public class BatchWordCountExample {

	public Map<String, Integer> getWordCount(String... lines) throws Exception {

		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		DataSet<String> data = env.fromElements(lines);
		List<Tuple2<String, Integer>> resultTuple2List = doWordCount(data).collect();
		return resultTuple2List
				.stream()
				.collect(Collectors.toMap(tuple -> tuple.f0, tuple -> tuple.f1));
	}

	private DataSet<Tuple2<String, Integer>> doWordCount(DataSet<String> words) {
		return words.flatMap(new Tokenizer()).groupBy(0).aggregate(Aggregations.SUM, 1);
	}
}
