# flink-poc

**What is Apache Flink ?**

![what_is_flink](images/what_is_flink.png)

# Core Building Blocks of Flink

![flink_core_building_blocks](images/flink_core_building_blocks.png)

# Bounded and Unbounded Streams

![stream](images/stream.png)

# Flink API

![flink_api](images/flink_api.png)

![layered_api](images/layered_api.png)

# Architecture

![flink_architecture](images/flink_architecture.png)

# Use Cases

Most common types of applications that are powered by Flink and give pointers to real-world examples.

* Event-driven Applications e.g. Fraud detection
* Data Analytics Applications e.g. Monitoring of telecom networks
* Data Pipeline Applications e.g. E-commerce search indexing/dashboards

![use_cases](images/use_cases.png)

# Integrations

![integrations](images/integrations.png)

# Spark vs Flink

![spark_vs_flink](images/spark_vs_flink.png)

# Flink Windows

**Keyed Windows**

stream
*        .keyBy(...)               <-  keyed versus non-keyed windows
*        .window(...)              <-  required: "assigner"
*       [.trigger(...)]            <-  optional: "trigger" (else default trigger)
*       [.evictor(...)]            <-  optional: "evictor" (else no evictor)
*       [.allowedLateness(...)]    <-  optional: "lateness" (else zero)
*       [.sideOutputLateData(...)] <-  optional: "output tag" (else no side output for late data)
*        .reduce/aggregate/fold/apply()      <-  required: "function"
*       [.getSideOutput(...)]      <-  optional: "output tag"\

**Non-Keyed Windows**

stream
*        .windowAll(...)           <-  required: "assigner"
*       [.trigger(...)]            <-  optional: "trigger" (else default trigger)
*       [.evictor(...)]            <-  optional: "evictor" (else no evictor)
*       [.allowedLateness(...)]    <-  optional: "lateness" (else zero)
*       [.sideOutputLateData(...)] <-  optional: "output tag" (else no side output for late data)
*        .reduce/aggregate/fold/apply()      <-  required: "function"
*       [.getSideOutput(...)]      <-  optional: "output tag"

**Window Assigners**

* Tumbling Windows
* Sliding Windows
* Session Windows
* Global Windows

![flink_windows](images/flink_windows.png)

**Window Functions**

* ReduceFunction
* AggregateFunction
* FoldFunction
* ProcessWindowFunction
* ProcessWindowFunction with Incremental Aggregation
* Using per-window state in ProcessWindowFunction
* WindowFunction (Legacy)

# Joins

**Tumbling Window Join**

![tumble_join](images/tumble_join.png)

**Sliding Window Join**

![sliding_join](images/sliding_join.png)

**Session Window Join**

![session_join](images/session_join.png)

**Interval Join**

![interval_join](images/interval_join.png)